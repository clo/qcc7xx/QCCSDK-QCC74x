-e Release Date: 2024-12-31
Release Author: zhuang
Git Commit Version: 4460fbfd59a4744063c3b10cf25b757977b8679e

Submodule Information:
 555f84735aecdbd76a566cf087ec8425dfb0c8ab ../../../components/crypto/mbedtls/mbedtls (v2.28.7)
 651baaebc6db49d2cbde57ff6ef5095c3c67154c ../../../components/fs (remotes/origin/qualcomm_release)
 8caca60d37d895f16d9ac2d3e9488b6c9c9ae17b ../../../components/graphics/lvgl (lvgl-8.3.11-4-g8caca60)
 d03dbcf6a8a6172da8139e499716314f045be6fb ../../../components/multimedia (remotes/origin/qualcomm_release)
 13f79d902df2c250ef41788c702c7011ddcc14c5 ../../../components/net/lwip/lwip (remotes/origin/qualcomm_release)
 8ab420516608f8a04d86b4f06980efa175240f57 ../../../components/net/netbus/host_router (remotes/origin/master_qualcom)
-01698f5ed572b2850679a75e8621bd2cfabe90a7 ../../../components/usb/cherryusb
 c9550b4b01eb72ec86ddf48a528cb8ecd7a2fd07 ../../../components/wireless/bluetooth (remotes/origin/qualcomm_release)
 ec3c4ff2bdeda7bdeb74e537d5336f616ccfd2a8 ../../../components/wireless/lmac154 (remotes/origin/qualcomm_release)
 56085726504d533dd7e4e815e48c3c8d21be5c31 ../../../components/wireless/matter/mfd (remotes/origin/qualcomm_release)
 716f0707317b6eddcf4e7078ed966222d9e78b8f ../../../components/wireless/qcc74x_macsw (remotes/origin/qualcomm_release)
 b068ee8c8960f9297f4232280bc89666cc60695c ../../../components/wireless/thread (remotes/origin/qualcomm_release)
 4cb926f73bdfc342d93a13d48f065f42c4653a47 ../../../components/wireless/wifi6 (amazon_release_v0.1.1-153-g4cb926f7)
 da9bfb89ca3ccb79252b3c917acfac76201ed754 ../../../drivers/lhal (lhal-v1.2.0-22-gda9bfb8)
 5f0e6c18607c62ea20489d9e5aae6439a4e28e3c ../../../drivers/soc/qcc743/phyrf (v1.0.3-14-g5f0e6c1)
 d688e8f9bb4396a1ec60020b65888d25ac94bcab ../../../drivers/soc/qcc743/std (d688e8f)
 90e8e1fc72c6654f3bfdac36e20dabaceca23f1b ../../../drivers/sys (remotes/origin/qualcomm_release)
 55687b0cebb026e7223f05549bbbe469cf6ffb97 ../../../tools/autotest (55687b0)
 940225ece91ca9b05606b68a4f439cded3fe28d9 ../../../tools/qcc74x_tools/QConn_Flash (v1.0.3-116-g940225e)
 d741e8a1bd6cec6b4fd0b7c72414518c2e2b63e9 ../../../tools/qcc74x_tools/QConn_Mem_Map (remotes/origin/qcc74x_bin)
 6a549135913378549408e0c636bc050eedac2f77 ../../../tools/qcc74x_tools/QConn_Production_Tool (v1.8.8-30-g6a54913)
 9a1e7be437bea131d2ab7e9d9bedb8e1297f4516 ../../../tools/qcc74x_tools/QConn_RCT (heads/master-6-g9a1e7be)
 efe346385b228c525b0bced4d4011e739d0f5914 ../../../tools/qcc74x_tools/QConn_Secure (remotes/origin/qcc74x_bin)

